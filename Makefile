.PHONY: validate clean

validate: deploy
	terraform validate

clean:
	rm -f getCar/getCar addCars/addCars plan
	rm -rf out

destroy:
	terraform destroy

init:
    # Only if not running in CI/CD
	terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${PROJECT_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${PROJECT_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${PROJECT_NAME}/lock" \
    -backend-config="username=${USERNAME}" \
    -backend-config="password=${TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"

plan:
	terraform plan -out=plan

deploy: init build plan
	terraform apply plan

build:
	mkdir out
	cd getCar && GOOS=linux GOARCH=amd64 go build && zip -r getCar.zip getCar && mv getCar.zip ../out
	cd addCars && GOOS=linux GOARCH=amd64 go build  && zip -r addCars.zip addCars && mv addCars.zip ../out
	aws s3 sync out/ s3://${S3_BUCKET}/

test:
	./test_api.sh
