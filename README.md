# Car API

API for adding cars to a database and retrieving those cars' information.

# Build status
[![pipeline status](https://gitlab.com/dougkirkley/car-api/badges/master/pipeline.svg)](https://gitlab.com/dougkirkley/car-api/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need AWS env variables added to the CI variables for the project

The AWS credentials used during testing was the administrator group policy in AWS.

Local development will need AWS credentials setup and awscli
![Screen_Shot_2020-07-14_at_10.34.36_PM](/uploads/0861c01be8a1b316637a8119fe4b61b0/Screen_Shot_2020-07-14_at_10.34.36_PM.png)

### Installing Locally

To install for local development on Mac via Homebrew
```
brew install awscli

brew install go

brew install terraform
```

## Testing the API

The CI stage **test** is for testing the adding, listing all, getting one item, and trying to fetch an item that does not exist.

You can run testing locally via `make test`

## Deployment

To deploy the API, add the AWS vars to the CI vars section shown above. Then, go to CI/CD and click "Run Pipeline".

**Do not run both CI and locally. They both use the same path for the Terraform state and will not work correctly.**

### Local Deployment
For local deployment only, you will need to create the s3 bucket manually

You will need to export the following variables for local deployment to work properly:
- $PROJECT_NAME
- $PROJECT_ID
- $USERNAME
- $TOKEN
- $S3_BUCKET

To deploy, run `make PROJECT_NAME=$PROJECT_NAME PROJECT_ID=$PROJECT_ID USERNAME=$USERNAME TOKEN=$TOKEN S3_BUCKET=$S3_BUCKET` 

in the root directory of the project.

## Authors

* **Douglass Kirkley** - *Initial work* - [car-api](https://gitlab.com/dougkirkley)
